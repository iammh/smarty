// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var EmailConfig = require('./email');
var nodemailer = require('nodemailer');
// load up the user model
var User            = require('../models/users');
var Profile = require('../models/profile');
var ProfileFunctions = require('../secrets/profile');
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
var SocialOath = require(__dirname+'/social-auth');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
// expose this function to our app using module.exports
module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            var u = {
              _id: user._id,
              local: {
                email: user.local.email
              },
              profile: user.profile,
              role: user.role
            }
            done(err, u);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password ,done) {
        if(!validateEmail(email)) {
          return (done(new Error("Not a email address")))
        }
        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {
          console.log("On signup");
          console.log(req.body);
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'local.email' :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
            } else {

                var name = {
                  firstname: req.body.firstname,
                  lastname: req.body.lastname
                };
                // if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user's local credentials
                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password);
                newUser.role = req.body.usertype;
                newUser.profile.name = name;
                newUser.local.verificationToken = EmailConfig.generateToken();
                // save the user
                newUser.save(function(err) {
                    if (err) {
                        throw err;
                    }

                    //email Sending
                    EmailConfig.registerSuccessEmail(newUser.local.email, newUser.local.verificationToken);
                    return done(null, newUser);
                });

            }

        });

        });

    }));

    // =========================================================================
// LOCAL LOGIN =============================================================
// =========================================================================
// we are using named strategies since we have one for login and one for signup
// by default, if there was no name, it would just be called 'local'

passport.use('local-login', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, email, password, done) { // callback with email and password from our form

    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ 'local.email' :  email }, function(err, user) {
        // if there are any errors, return the error before anything else
        if (err)
            return done(err);

        // if no user is found, return the message
        if (!user)
            return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash

        // if the user is found but the password is wrong
        if (!user.validPassword(password))
            return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

        // all is well, return successful user
        return done(null, user);
    });

}));

//GOOGLE LOGIN
passport.use(new GoogleStrategy({

        clientID        : SocialOath.googleAuth.clientID,
        clientSecret    : SocialOath.googleAuth.clientSecret,
        callbackURL     : SocialOath.googleAuth.callbackURL,

    },
    function(token, refreshToken, profile, done) {
      console.log(profile);
      process.nextTick(function () {
        User.findOne({'google.id': profile.id}, function (err, savedInstance) {
          if(err) {
            done(err);
          }else if(savedInstance) {
            done(null, savedInstance)
          }else {
            var newUser          = new User();

                    // set all of the relevant information
                    newUser.google.id    = profile.id;
                    newUser.google.token = token;
                    newUser.google.name  = profile.displayName;
                    newUser.profile.name.firstname  = profile.displayName;
                    newUser.google.email = profile.emails[0].value; // pull the first email
                    newUser.role = "employee";
                    newUser.profile.avatar.url = profile._json.image.url;
                    newUser.local.verificationToken = EmailConfig.generateToken();
                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        EmailConfig.registerSuccessEmail(newUser.local.email, newUser.local.verificationToken);
                        return done(null, newUser);
                    });
          }
        })
      });
    }));

};
