module.exports = {

    'facebookAuth' : {
        'clientID'      : 'your-secret-clientID-here', // your App ID
        'clientSecret'  : 'your-client-secret-here', // your App Secret
        'callbackURL'   : 'http://localhost:8080/auth/facebook/callback'
    },

    'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'      : '64673971914-cnmnktimu5gf9otq05idb03n4i219poc.apps.googleusercontent.com',
        'clientSecret'  : 'qHN8RQl-mzufYWQxUMZzY3iE',
        'callbackURL'   : 'http://ec2-52-87-252-209.compute-1.amazonaws.com/auth/google/callback'
    }

};
