var mongoose = require('mongoose')
    ,Schema = mongoose.Schema;

var skillSchema = new Schema({
  title: String
});

module.exports = mongoose.model('skills', skillSchema);
