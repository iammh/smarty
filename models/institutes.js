var mongoose = require('mongoose')
    ,Schema = mongoose.Schema;

var instituteSchema = new Schema({
  name: String
});

module.exports = mongoose.model('institutes-education', instituteSchema);
