var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var companySchema = new Schema({
  name: {type: String , required: true},
  tagline: String,
  logo: String,
  description: String,
  addresses: [{
    addressLine1: String,
    addressLine1: String,
    city: String,
    country: String
  }],
  contacts: [
    {phone: String, email: String, pager: String, fax: String}],
  type: String,
  keyword: [String],
  superviser: Object,
  smartUrl: {type: String, unique: true }
});



module.exports = mongoose.model("smarty_companies", companySchema);
