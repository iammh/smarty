// app/models/user.js
// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({

    local            : {
        email        : String,
        password     : String,
        verificationToken: String
    },
    facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    role: String,
    profile: {
      name: {
        firstname: {type:String, default: ''},
        lastname: {type:String, default: ''},
      },
      phone: String,
      contact: {
        location: {
          longitude: {type: Number, default: 0.0},
          latitude: {type: Number,default: 0.0},
        },
        address: {type:String, default: ''},
        country: {type:String, default: ''}
      },
      birthday: {type: Date, default: new Date() },
      avatar: {
        _id: Object,
        url: {type: String, default: 'https://conferencecloud-assets.s3.amazonaws.com/default_avatar.png'},
        file: Object
      }
      ,overview: String
    }

});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};


// create the model for users and expose it to our app
module.exports = mongoose.model('users', userSchema);
