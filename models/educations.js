var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var experienceSchema = new Schema({
  title: String,
  institute: String,
  start: Date,
  end: Date,
  description: String,
  grade: String,
  user: String
});


module.exports = mongoose.model('educations', experienceSchema);
