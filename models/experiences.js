var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var experienceSchema = new Schema({
  title: String,
  company: String,
  start: Date,
  end: Date,
  description: String,
  user: String
});


module.exports = mongoose.model('experiences', experienceSchema);
