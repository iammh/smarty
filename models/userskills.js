var mongoose = require('mongoose')
    ,Schema = mongoose.Schema;

var userSkillSchema = new Schema({
  title: String,
  verified: {
    type: Boolean,default: false
  },
  user: String
});

// userSkillSchema.methods.checkDuality = function (user, title) {
//
// }

module.exports = mongoose.model('userskills', userSkillSchema);
