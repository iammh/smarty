var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var projectSchema = new Schema({
  name: String,
  url: String,
  image: String

});

var CVSchema = new Schema({
  owner: String,
  introduction: {
    coverStory: String,
    video: String,
    coverImage: String
  },
  degrees: [
    {
      title: String,
      result: String,
      institute: String,
      duration: {
        start: String,
        end: String
      },
      descrition: String,
      area: String

    }
  ],
  jobs: [
    {
      title: String,
      company: String,
      duration: {
        start: String,
        end: String
      },
      descrition: String
    }
  ],
  skills: [String],

  projects: [projectSchema]

});



module.exports = mongoose.model('cvdocuments', CVSchema);
