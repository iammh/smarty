var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var avatarSchema = new Schema({
  url: {type: String,unique: true },
  file: Object,
  owner: String
});

module.exports = mongoose.model('useravatar', avatarSchema);
