var path = require('path');
var __BASEDIR = path.resolve('.');
var express = require('express');
var profile = express.Router();
var async = require('async');

var ProfileFunctions = require(__BASEDIR+ '/secrets/profile');
var Users = require(__BASEDIR+ '/models/users');
var Experiences = require(__BASEDIR+ '/models/experiences');
var Avatars = require(__BASEDIR+ '/models/avatars');
var multer = require('multer');
var Profiles = require(__BASEDIR+ '/models/cv');
var Educations = require(__BASEDIR+ '/models/educations'); //educations model
var UserSkills = require(__BASEDIR+ '/models/userskills'); //skill model
var fs = require('fs');
// var upload = multer({ dest: 'uploads/' }).single('avatar');


profile.use(function (req, res, next) {
  if(req.isAuthenticated()) {
    if (req.user.role === "employee") {
      next();
    } else {
      res.redirect('/');
    }

    // var info = {
    //   user: req.user,
    //   profile:
    // }
  }else {
    res.redirect('/login');
  }
});


var getFTypeName = function (mime) {
  // switch (mime) {
  //   case "image/jpeg":
  //     return "jpg";
  //     break;
  //   default:
  //     return "file";
  // }
  var re = /\s*\/\s*/;
  var nameList = mime.split(re);
  // console.log(nameList);
  return nameList[1];
}

var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, __BASEDIR + '/uploads/');
  },
  filename: function (req, file, callback) {
    var previousAvData = req.user.profile.avatar._id ? req.user.profile.avatar._id.toString() : null;
    var previousFileName = req.user.profile.avatar.url ? __BASEDIR +req.user.profile.avatar.url : null;
    console.log("Previous Name "+previousFileName, previousAvData);

    if(previousAvData) {
      Avatars.remove({owner: req.user._id}, function (err,response) {
        console.log(err);
        if(!err) {
          fs.unlink(previousFileName, function (errDelete) {
            if(errDelete) {
              console.log("Deletion failed"+errDelete);
              // throw new Error(errDelete);
            }
            else {
              console.log("Deleted Previous AVATAR");
              var customFileName = 'image-user-'+ Date.now() + '-' + Math.round(Math.random(999,10000000) * 100000000) + "."+ getFTypeName(file.mimetype);

              var avatar = new Avatars();
              avatar.url = '/uploads/'+ customFileName;
              avatar.storedFileName = customFileName;
              avatar.file = file;
              avatar.owner = req.user._id;
              avatar.save(function (err,doc) {
                // console.log(err);
                if(err) {
                  console.log(err);
                }else {
                  console.log("image FIle"+ doc);

                  Users.update({_id: req.user._id}, {
                    $set: {
                      'profile.avatar': doc
                    }
                  },function (err,c) {
                    console.log(err);
                    if(err) {
                      console.log("Update failed" + err);
                      throw new Error(err);
                    }
                    return callback(null, customFileName);
                  }); //users update
                }
              }); //new save
            }
          });
        }else {
          console.log("ERROR DELETING AVATARA DATA",response);
        }
        // console.log(response);
      });
    } else {
      var customFileName = 'image-user-'+ Date.now() + '-' + Math.round(Math.random(999,10000000) * 100000000) + "."+ getFTypeName(file.mimetype);

      var avatar = new Avatars();
      avatar.url = '/uploads/'+ customFileName;
      avatar.storedFileName = customFileName;
      avatar.file = file;
      avatar.owner = req.user._id;
      avatar.save(function (err,doc) {
        // console.log(err);
        if(err) {
          console.log(err);
        }else {
          console.log("image FIle"+ doc);

          Users.update({_id: req.user._id}, {
            $set: {
              'profile.avatar': doc
            }
          },function (err,c) {
            console.log(err);
            if(err) {
              console.log("Update failed" + err);
              throw new Error(err);
            }
            return callback(null, customFileName);
          }); //users update
        }
      }); //new save
    }



  }
});
var fileFilter = function (req, file , cb) {
  console.log(getFTypeName(file.mimetype));
  var extension = getFTypeName(file.mimetype).toString();
  // if (extension !== 'jpeg' || extension !== 'jpeg') {
  //   console.log("Only jpg are allowed");
  //   return cb(new Error('Only jpg are allowed'));
  // }
  if(extension === 'jpg' || extension === 'jpeg') {
    cb(null, true);
  }else if(extension === 'png') {
    cb(null, true);
  }else {
    return cb(new Error('Only jpg are allowed'));
  }

}
var uploadOptions = {
  storage: storage,
  fileFilter: fileFilter,
  limits: {
    fileSize: 4194304
  }
};
var upload = multer(uploadOptions);

  profile.get('/', function (req , res) {

      res.redirect('/profile/'+ req.user._id);


  });
  profile.get('/name', function (req,res) {

      res.render('editName');


  });

  profile.post('/avatar',upload.single('avatar'),function (req, res, next) {

    Users.findOne({_id: req.user._id}, function (err, doc) {
      if(!err) {
        res.send(doc.profile.avatar.url);
      }
    })

    // res.redirect('/profile');

  });

  profile.post('/name', function (req,res) {

      ProfileFunctions.setProfileName(req,res);


  });

  profile.post('/contact', function (req,res) {

      ProfileFunctions.setContactDetails(req,res);


  });
  profile.post('/birthday', function (req,res) {

      ProfileFunctions.setBirthday(req,res);


  });



  // Users.findOne({_id: params},function (err, user) {
  //   if(!err && user) {
  //     res.render('pages/cv', {title: "Smarty",profile: user.profile,user: req.user, self: false});
  //   }else {
  //     res.render('pages/cv', {title: "Smarty",profile: null,user: req.user, self: false});
  //
  //   }
  // });



  profile.get('/:_id',function (req , res) {
      var params = req.params._id;
      var profileById = req.user.profile;
      var self = true;

        async.parallel({
          user: function (callback) {
            Users.findOne({_id: params},function (err, user) {
              if(!err && user) {
                callback(null,user)
                // res.render('pages/cv', {title: "Smarty",profile: user.profile,user: req.user, self: false});
              }else {
                // res.render('pages/cv', {title: "Smarty",profile: null,user: req.user, self: false});
                callback(null,null);
              }
            });

          },
          experiences: function (callback) {
            Experiences.find({user: params}, function (err, exp) {
              callback(null,exp)
            })
          },
          educations: function (callback) {
            Educations.find({user: params}, function (err,educations) {
              if(!err && educations) {
                callback(null,educations)
                // res.render('pages/cv', {title: "Smarty",profile: user.profile,user: req.user, self: false});
              }else {
                // res.render('pages/cv', {title: "Smarty",profile: null,user: req.user, self: false});
                callback(null, null);
              }
            })
          },
          skills: function (callback) {
            UserSkills.find({user: params}, function (err, skillSet) {
              console.log(skillSet);
              if(!err) {
                callback(null, skillSet);
              }else {
                callback(null, null);
              }
            });
          }
        },function (err, result) {
          console.log(result);
          if(!err) {
            if(req.user._id.toString() !== params ) {
              if(result.user) {
                res.render('pages/cv', {title: "Smarty",profile: result.user.profile,user: req.user, experiences: result.experiences, educations: result.educations, skills: result.skills, self: false});
              }else {
                res.render('pages/cv', {title: "Smarty",profile: null,user: req.user, experiences: result.experiences, educations: result.educations, skills: result.skills, self: false});

              }
            }else {
              res.render('pages/cv', {title: "Smarty",profile: profileById,user: req.user, experiences: result.experiences, educations: result.educations,skills: result.skills,self: true});
            }
          }
        });


  });

  profile.post('/overview', function (req , res) {
    if(req.isAuthenticated()) {
      var overview = req.body.overview;
      // console.log(overview);
      if(overview.length > 100) {
        Users.update({_id: req.user._id}, {
          $set: {
            'profile.overview': overview
          }
        }, function (err, doc) {
          if(err) {
            res.json({success: false});
          }
          else {
            res.json({success: true});
          }
        });
      }else {
        res.json({success: false, error: "did not worked. Please elaborate it"});
      }
    }
  });
module.exports = profile;
