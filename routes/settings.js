var express = require('express'),
    settings = express.Router();

settings.use(function (req,res,next) {
  if(req.isAuthenticated()) {
    next();
  }
  else {
    throw new Error("You are not logged in");
  }
})

settings.get('/', function (req, res) {
  res.render('pages/settings', {title: "settings", user: req.user});
});


module.exports = settings;
