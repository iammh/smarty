var express = require('express');
var Router = express.Router();
var path = require('path');
var BASE = path.resolve(".");
var Company = require(BASE + '/models/companies.js');


Router.get('/:_id', function (req, res) {
  // res.json({name: 'companies '+ req.params._id});
  Company.findOne({_id: req.params._id}, function (err, result) {
    if(!err) {
      res.render('pages/company/home' , {company: result});
    }
  })

});


Router.use(function (req , res, next) {
  res.redirect('/');
});

module.exports = Router;
