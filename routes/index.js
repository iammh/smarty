var path = require('path');
var BASE = path.resolve(".");
var Users = require(BASE + '/models/users.js');
var Company = require(BASE + '/models/companies.js');
var ifComany = function (req, res, next) {
  if(req.user.role === 'company') {
    next()
  }
  else {
    res.redirect('/');
  }
}
var router = function (app,passport) {

  app.get('/', function (req, res) {
    user = req.user ? req.user : null;

    res.render('pages/index', {title: "Smarty",user: user , message: req.flash.message});
    res.flush();
  });

  app.get('/company', ifComany ,function (req, res) {
    // res.json({name: 'companies '+ req.params._id});
    res.render('pages/company/mycompanies');
  });
  app.get('/company/create', ifComany ,function (req, res) {
    // res.json({name: 'companies '+ req.params._id});
    res.render('pages/company/create');
  });
  app.post('/company/create', ifComany ,function (req, res) {
    // res.json({name: 'companies '+ req.params._id});
    // res.render('pages/company/create');
    var company = new Company();

    company.name = req.body.name;
    company.smartUrl = req.body.smartUrl;
    company.save(function (err , savedInstance) {
      if(!err) {
        res.redirect('/companies/'+ savedInstance._id);
      }
      else {
        res.render('pages/company/create', {error: err});
      }
    });
  });

var join = require( __dirname+'/auth.js')(app,passport);
// var profile = require( __dirname+'/profile.js')(app);
  // var login = require(__dirname + '/login.js')(app);
}

module.exports = router;
