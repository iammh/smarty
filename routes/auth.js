
var path = require('path');
var __BASEDIR = path.resolve('.');
var Users = require(__BASEDIR + '/models/users.js');
var passport = require('passport');
var LocalStrategy = require('passport-local');
// var config = require('../config/passport.js');
(function () {
  module.exports = function (app, passport) {

    app.get('/login',function (req , res) {
      if(!req.isAuthenticated()) {
        res.render('pages/login' ,{title: "Smarty", message: req.flash('loginMessage'), user: req.user});
      }else {
        res.redirect('/');
      }
    });
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));


    app.get('/join', function (req , res) {
      if(!req.isAuthenticated()) {
        res.render('pages/join',{title: "Smarty", message: req.flash('signupMessage') , user: req.user});
      }else {
        res.redirect('/');
      }
    });


  // process the signup form
    app.post('/join', passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/join', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    //password change
    app.post('/', function (req, res) {
      
    })

    //google login

    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    successRedirect : '/profile',
                    failureRedirect : '/'
            }));


  // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    // route middleware to make sure a user is logged in
    function isLoggedIn(req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the home page
        res.redirect('/');
    }

  }
})()
