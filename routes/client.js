var express = require('express'),
    client = express.Router(),
    ProfileFunctions = require('../secrets/profile'),
    Profile = require('../models/cv'),
    Institutes = require('../models/institutes');

client.use(function (req,res, next) {

        if(req.isAuthenticated()) {
          next();
          // var info = {
          //   user: req.user,
          //   profile:
          // }
        }else {
          res.redirect('/login');
        }
});

client.get('/', function (req, res) {
  // Profile.findOne({profile_of: req.user._id}, function (err,doc) {
  //   console.log(err);
  //   console.log(doc);
  // });
  res.render('pages/profile', {title: "Smarty",profile: req.user.profile,user: req.user});

});

client.get('/institutes/json', function(req, res) {
  Institutes.find(function (err, docs) {
    var institutes = [];
    if (!err) {
      institutes = docs;
    }
    // res.render('pages/admin/institutes', {user: req.user, title: 'Admin Site :: Institutes', institutes: institutes});
    res.json(docs);
  });
});

client.get('/mycv', function (req , res) {
  Profile.findOne({_of_user: req.user._id}, function (err, cv) {
    if(!err && !cv) {
      res.render('pages/cv', {title: "Smarty: MyCV", user: req.user, cv: null});
    }
    else if(err){
      throw new Error(err);
    }
    else {
      res.render('pages/cv', {title: "Smarty: MyCV", user: req.user, cv: cv});

    }
  })

});
module.exports = client;
