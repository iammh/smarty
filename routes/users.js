var express = require('express');
var Users = require('../models/users'); //users model
var Experiences = require('../models/experiences'); //experiences model
var Educations = require('../models/educations'); //educations model
var UserSkills = require('../models/userskills'); //skill model
var users = express.Router(); //users router


users.get('/', function (req , res) {
  res.json({done: true});
});
users.get('/:_id', function (req , res) {
  res.json(req.user);
});

users.get('/experiences/byuser/:user', function (req,res) {
  Experiences.find({user: req.params.user}, function (err, experiences) {
    if(!err) {
      req.json(experiences);
    }
  });
});
users.get('/experience/:id', function (req,res) {
  Experiences.findOne({_id: req.params.id}, function (err, experience) {
    if(!err) {
      res.json(experience);
    }
  });
});



users.post('/experiences', function (req, res) {
  console.log(req.body);
  var experience = new Experiences();

  var reqBody = req.body;
  experience.title = reqBody.title;
  experience.company = reqBody.company;
  experience.start = new Date(reqBody.start);
  experience.end = new Date(reqBody.end);
  experience.description = reqBody.description;
  experience.user = req.user._id;

  experience.save(function (err, doc) {
    if(!err) {
      console.log("Created", doc);
      res.json(doc)
    }
  });
});
users.put('/experiences', function (req, res) {
  var experience = new Experiences();
  var reqBody = req.body;
  console.log(req.body);

  Experiences.update({_id: reqBody._id}, {
    $set: {
      title: reqBody.title,
      company: reqBody.company,
      start: reqBody.start,
      end: reqBody.end,
      description: reqBody.description
    }
  }, function (err,update) {
      if(!err) {
        res.json(update);
      }
  });
});


users.delete('/experiences', function (req, res) {
  var id = req.body.id;
  console.log(req.body);

  Experiences.remove({_id: id}, function (err, success) {
    if(!err) {
      res.json(success);
    }
  });
});

users.post('/educations', function (req, res) {
  var reqBody = req.body;
  console.log(reqBody);
  var education = new Educations();
  education.title = reqBody.title;
  education.institute = reqBody.institute;
  education.grade = reqBody.grade;
  education.start = reqBody.start;
  education.end = reqBody.end;
  education.description = reqBody.description;
  education.user = req.user._id;

  education.save(function (err, savedInstance) {
    if(!err) {
      res.json(savedInstance);
    }
  });
});


users.get('/education/:id', function (req,res) {
  Educations.findOne({_id: req.params.id}, function (err, education) {
    if(!err) {
      res.json(education);
    }
  });
});

users.put('/educations', function (req, res) {

  var reqBody = req.body;
  console.log(req.body);

  Educations.update({_id: reqBody._id}, {
    $set: {
      title : reqBody.title,
      institute : reqBody.institute,
      grade : reqBody.grade,
      start : reqBody.start,
      end : reqBody.end,
      description : reqBody.description
    }
  }, function (err,update) {
      if(!err) {
        // console.log("");
        res.json(update);
      }
  });
});
users.delete('/educations', function (req, res) {

  var id = req.body.id;
  console.log(req.body);

  Educations.remove({_id: id}, function (err, success) {
    if(!err) {
      res.json(success);
    }
  });
});
var checkDuality = function (user, title , callback) {
  console.log(user, title);
  UserSkills.find({user: user, title: title}, function (err, doc) {
    if(!err) {
      console.log(doc.length);
      if(doc.length > 0) {
        callback(true);
      }else {
        callback(false);
      }
    }
  })
}
users.post('/skills', function (req, res) {
  console.log(req.body);
  checkDuality(req.user._id, req.body.skill, function (result) {
    if(!result) {
      var userSkill = new UserSkills();
      userSkill.title = req.body.skill;
      userSkill.user = req.user._id;
      userSkill.save(function (err, savedInstance) {
        if(!err) {
          res.json(savedInstance);
        }
      });
    }else {
      res.json({error: "Duality..."});
    }
  });

});



users.delete('/skills', function (req, res) {
  UserSkills.remove({_id: req.body.id}, function (err, success) {
    res.json(success);
  });
});
module.exports = users;
