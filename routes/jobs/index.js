var express = require('express'),
    Router = express.Router();


Router.use(function (req , res, next) {
  if(req.isAuthenticated()) {
    next();
  }
  else {
    res.redirect('/login');
  }
});
Router.get('/jobs', function (req , res) {
  if(req.user.role === 'company') {
    //fetch company jobs and display
    res.render('pages/jobs/companyjobs');
  }else {
    res.render('pages/jobs/jobs');
  }
});

Router.get('/job', function (req , res) {
  if(req.user.role === 'company') {
    res.render('pages/jobs/posts');
  }else {
    res.redirect('/jobs');;
  }
});

module.exports = Router;
