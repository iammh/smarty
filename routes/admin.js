var express = require('express')
  , router = express.Router()
  , Institutes = require('../models/institutes');


router.use(function (req, res, next) {
  console.log(req.user);
  if(req.isAuthenticated() && req.user.role === 'admin') {
    next();
  }
  else {
    throw new Error("Access Denied");
  }
});

// Car brands page
router.get('/', function(req, res) {
  res.render('pages/admin/home', {user: req.user, title: 'Admin Site'});

});

//institutes route
router.get('/institutes', function(req, res) {
  Institutes.find(function (err, docs) {
    var institutes = [];
    if (!err) {
      institutes = docs;
    }
    res.render('pages/admin/institutes', {user: req.user, title: 'Admin Site :: Institutes', institutes: institutes});

  });
});

router.post('/institutes', function(req, res) {
  var institute = new Institutes();
  institute.name = req.body.name;
  institute.save(function (err,doc) {
    res.json(doc);
  });
});
//end of institutes route
// Car models page
router.get('/models', function(req, res) {
  res.send('Audi Q7, BMW X5, Mercedes GL')
});

router.get('*', function (req,res) {
  throw new Error("No route found..");
});

module.exports = router
