var path = require('path');
var __BASEDIR = path.resolve('.');
var bycrypt = require('bcrypt'),
    Q = require('q'),
    Users = require(__BASEDIR + '/models/users.js');


module.exports.localRegistration = function (email, password) {
  var deffered = Q.defer();
  Users.findOne({email: email}, function (err, results) {
    console.log(results);
    if(results) {
      console.log("USER ALREADY AVAILABLE");
      deffered.resolve(false); // user available
    }
    else if (err) {
      console.log("Error Registering new User", err);
      deffered.reject(new Error(err));
    }
    else {
      console.log("Creating");
      var hash = bycrypt.hashSync(password, 8);
      console.log("ON localRegistration", hash);
      var user = {
        email: email,
        password: hash
      };
      var welcome = new Users(user);
      welcome.save(function (err, userSaved) {
        if(!err) {
          deffered.resolve(userSaved);
        }
        else {
          deffered.reject(new Error(err));
        }
      });
    }
  });//userInsertion

  return deffered.promise;
}

module.exports.localSignin = function (email, password) {
  var deffered = Q.defer();

  Users.findOne({email: email}, function (err, user) {
    if(!err) {
      if(user) {
          var hash = user.password;
          console.log(hash);
          if(bycrypt.compareSync(password, hash)) {
            console.log("Found User", user);
            deffered.resolve(user);
          }else {
            console.log("Password Does not match");
            deffered.resolve(false);
          }
      }
      else {
        console.log("User Not Found");
        deffered.reject(new Error(err));
      }
    }
  });//finding and checking
  return deffered.promise;
}
