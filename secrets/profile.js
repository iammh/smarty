var Users = require('../models/users');
var create = function (user) {
  var profile = new Profile();

  profile.profile_of = user._id;
  profile.save(function (error) {
    if (error) {
        throw err;
    }
    return;
  });
}
module.exports.setProfileName = function (req,res) {
  console.log("ON NAME CHANGE", req.body);
  var user = req.user;
  Users.findOne({_id: user._id}, function (err, doc) {
    if(err) {
      console.log(err);
    }
    else if(!doc){
      console.log("Not found");
      // create(req.user);
      // this(req,res);
    }else {
      console.log("ON NAME", doc);
      Users.update({_id: doc._id}, {
        $set: {
          'profile.name': req.body
        }
      },function (err,doc) {
        console.log("Error on update",err);
        console.log("Success on update",doc);
      });
      res.redirect('/profile/'+req.user._id);
    }
  })
}

module.exports.setContactDetails = function (req,res) {
  console.log("ON NAME CHANGE", req.body);
  var user = req.user;
  Users.findOne({_id: user._id}, function (err, doc) {
    if(err) {
      console.log(err);
    }
    else if(!doc){
      console.log("Not found");
      // create(req.user);
      // this(req,res);
    }else {
      console.log("ON NAME", doc);
      Users.update({_id: doc._id}, {
        $set: {
          'profile.contact': {
            location: {
              longitude: req.body.long,
              latitude: req.body.lat
            },
            address: req.body.address,
            country: req.body.country
          }
        }
      },function (err,doc) {
        console.log("Error on update",err);
        console.log("Success on update",doc);
      });
      res.redirect('/profile/'+req.user._id);
    }
  })
}
module.exports.setBirthday = function (req,res) {
  console.log("ON NAME CHANGE", req.body);
  var user = req.user;
  Users.findOne({_id: user._id}, function (err, doc) {
    if(err) {
      console.log(err);
    }
    else if(!doc){
      console.log("Not found");
      // create(req.user);
      // this(req,res);
    }else {
      console.log("ON NAME", doc);
      Users.update({_id: doc._id}, {
        $set: {
          'profile.birthday': new Date(req.body.birthday)
        }
      },function (err,doc) {
        console.log("Error on update",err);
        console.log("Success on update",doc);
      });
      res.redirect('/profile/'+req.user._id);
    }
  })
}

// module.exports.create = create;
