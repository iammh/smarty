var express = require('express'),
     path = require('path'),
    __BASEDIR = path.resolve('.'),
    favicon = require('static-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    Handlebar = require('express-handlebars'),
    methodOverride = require('method-override'),
    mongoose = require('mongoose'),
    flash    = require('connect-flash'),
    // ConnectRoles = require('connect-roles'),
    passport = require('passport'),
    async = require('async');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const options = {
  url: 'mongodb://localhost:27017/socialmarket',
  touchAfter: 24 * 3600
};
var LocalStrategy = require('passport-local').Strategy;
var functions = require(__dirname + '/secrets/functions.js');
// var Roles = new ConnectRoles({
//   failureHandler: function (req, res, action) {
//     var accept = req.headers || '';
//     res.status(403);
//     // console.log(accept);
//     if (~accept.accept.indexOf('html')) {
//       res.render('access-denied', {action: action})
//     }
//     else {
//       res.send('Access Denied- Permission: '+ action);
//     }
//   }
// });

// console.log(functions);
var app = express();
// view engine setup
// app.set('views', path.join(__dirname, 'views'));

var compress = require('compression');
function parallel(middlewares) {
  return function (req, res, next) {
    async.each(middlewares, function (mw, cb) {
      mw(req, res, cb);
    }, next);
  };
}
app.use(compress());
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(cookieParser());
app.use(session({secret: 'iammhshuvo' , saveUninitialized: true, resave: true,store: new MongoStore(options)}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions



app.use(flash());
app.use('/public',express.static(path.join(__dirname, 'public')), {maxAge: 86400000});
app.use('/bower_components/',express.static(path.join(__dirname, 'bower_components')), {maxAge: 86400000});

app.use('/uploads',express.static(path.join(__dirname, 'uploads')));
//General local variables

app.use(function (req , res , next) {
  res.locals.isAuthenticated = req.isAuthenticated();
  res.locals.user = req.user;
  res.locals.error = null;
  res.locals.title = "Smarty:" + req.path;

  next();
});
// app.use(parallel([
//   compress(),
//   favicon(),
//   bodyParser.json(),
//   bodyParser.urlencoded({ extended: false}),
//   methodOverride('X-HTTP-Method-Override'),
//   cookieParser(),
//   session({secret: 'iammhshuvo' , saveUninitialized: true, resave: true}),
//   passport.initialize(),
//   passport.session()
// ]));


// Session-persisted message middleware
app.use(function(req, res, next){
  var err = req.session.error,
      msg = req.session.notice,
      success = req.session.success;

  delete req.session.error;
  delete req.session.success;
  delete req.session.notice;

  if (err) res.locals.error = err;
  if (msg) res.locals.notice = msg;
  if (success) res.locals.success = success;

  next();
});




var ejs = require('ejs');
var engine = require('ejs-mate');
app.engine('ejs', engine);
// app.engine('handlebars', Handlebar({defaultLayout: 'main'}));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
mongoose.connect('mongodb://localhost:27017/socialmarket');
require('./config/passport')(passport);
var router = require(__dirname+ '/routes')(app,passport);
var adminRoute = require(__BASEDIR + '/routes/admin.js');
var clientRoute = require(__BASEDIR + '/routes/client.js');
var profileRoute = require(__BASEDIR + '/routes/profile');
var usersRoute = require(__BASEDIR + '/routes/users.js');
var settingsRoute = require(__BASEDIR + '/routes/settings.js');
var utilsRoutes = require(__BASEDIR + '/routes/utils.js');
var apiRoutes = require(__BASEDIR + '/routes/api/');
var companiesRoutes = require(__BASEDIR + '/routes/companies/');
var jobsRoutes = require(__BASEDIR + '/routes/jobs/');
app.use('/dashboard', adminRoute);
app.use('/app', clientRoute);
app.use('/profile', profileRoute);
app.use('/users', usersRoute);
app.use('/settings', settingsRoute);
app.use('/utils', utilsRoutes);
app.use('/api', apiRoutes);
app.use('/companies', companiesRoutes);
app.use('/bank', jobsRoutes);

app.get('/resources/countries', function (req,res) {
  var coun = [
  {"name": "C++", "code": "AF"},
  {"name": "C", "code": "AX"},
  {"name": "Javascript", "code": "AL"},
  {"name": "Python", "code": "DZ"},
  {"name": "Microsoft Word", "code": "AS"}
  ];
  res.json(coun);
});
/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('I could not find anything like you are looking for');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500 || 404);
        res.render('pages/error', {
            message: err.message,
            error: err,
            title: 'Smarty:: Error....'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500 || 404);
    res.render('pages/error', {
        message: err.message,
        error: err,
        user: req.user,
        title: 'Smarty:: Error'
    });
});



module.exports = app;
